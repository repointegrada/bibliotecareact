import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Subscribers from './subscribers/Subscribers';
import EditSubscriber from './subscribers/EditSubscriber';
import ShowSubscriber from './subscribers/ShowSubscriber';
import NewSubscriber from './subscribers/NewSubscriber';
import NavBar from './components/layout/Navbar'
import store from './redux/store'
import { Provider } from 'react-redux'
import Books from './components/books/Books'
import EditBook from './components/books/EditBook'
import LendBook from './components/books/LendBook'
import NewBook from './components/books/NewBook'
import ShowBook from './components/books/ShowBook'
import {
  UserIsAuthenticated,
  UserIsNotAuthenticated
} from '../src/helpers/auth'

import './App.css'
import Login from './components/auth/login';

function App() {
  return (
    <Provider store={store}>
      <Router>
        <NavBar />
        <div className="container">
          <Switch>
            <Route exact path="/" component={UserIsAuthenticated(Books)} />
            <Route exact path="/books/show/:id" component={UserIsAuthenticated(ShowBook)} />
            <Route exact path="/books/new" component={UserIsAuthenticated(NewBook)} />
            <Route exact path="/books/edit/:id" component={UserIsAuthenticated(EditBook)} />
            <Route exact path="/books/loan/:id" component={UserIsAuthenticated(LendBook)} />


            <Route exact path="/subscribers" component={UserIsAuthenticated(Subscribers)} />
            <Route exact path="/subscribers/new" component={UserIsAuthenticated(NewSubscriber)} />
            <Route exact path="/subscribers/show/:id" component={UserIsAuthenticated(ShowSubscriber)} />
            <Route exact path="/subscribers/edit/:id" component={UserIsAuthenticated(EditSubscriber)} />
            <Route exact path="/login" component={UserIsNotAuthenticated(Login)} />
          </Switch>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
