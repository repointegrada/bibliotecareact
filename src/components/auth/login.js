import React, { Component } from 'react';
import { Form, Input, Button } from 'antd';
import { firebaseConnect } from 'react-redux-firebase'

class Login extends Component {
  state = {
    email: '',
    password: ''
  }
  readData = e => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  signIn = e => {
    e.preventDefault()
    const { firebase } = this.props
    const { email, password } = this.state

    firebase.login({
      email,
      password
    })
      .then(resultado => console.log(resultado))
      .catch(error => console.log(error))
  }

  render() {
    return (
      <div>
        <h2>
          <i className="fas fa-lock"> Sign in</i>

          <Form onSubmit={this.signIn}>
            <Form.Item label="Email">
              <Input
                type="email"
                name="email"
                required
                value={this.state.email}
                onChange={this.readData}
              />
            </Form.Item>

            <Form.Item label="Password">
              <Input
                type="password"
                name="password"
                required
                value={this.state.password}
                onChange={this.readData}
              />
            </Form.Item>
            <Button type="primary" htmlType="submit" value="Sign in">
              Sign in
        </Button>
          </Form>
        </h2>
      </div>
    );
  }
}

export default firebaseConnect()(Login);