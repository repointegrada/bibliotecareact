import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { Button, Table } from 'antd';
import { Link } from 'react-router-dom'
import Spinner from '../layout/Spinner'

const Books = ({ books, firestore }) => {

  if (!books) return <Spinner />

  /**
 * Se especifíca a que tabla y con que id se va a eliminar el registro
 * @param {*} id 
 */
  const deleteBook = id => {

    firestore.delete({
      collection: 'books', // Es la tabla
      doc: id // Es el where
    })
  }

  // Columnas para la tabla
  const columns = [
    {
      title: 'Title',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: 'ISBN',
      dataIndex: 'ISBN',
      key: 'ISBN',
    },
    {
      title: 'Editorial',
      dataIndex: 'editorial',
      key: 'editorial',
    },
    {
      title: 'Existence',
      dataIndex: 'existence',
      key: 'existence',
    },
    {
      title: 'Borrowed',
      dataIndex: 'borrowed',
      key: 'borrowed',
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      render: (text, data) => (
        <div>
          <Button type="primary">
            <Link to={`/books/show/${data.key}`}>
              <i className="fas fa-angle-double-right"></i> {''} More Info
        </Link>
          </Button>

          <Button type="danger" onClick={() => deleteBook(data.key)}>
            <i className="fas fa-trash-alt"></i> {' '} Delete
        </Button>
        </div>
      ),
    }
  ];

  const data = books.map(book => ({
    key: book.id,
    title: book.title,
    ISBN: book.ISBN,
    editorial: book.editorial,
    existence: book.existence,
    borrowed: [book.existence - book.borrowed.length]
  }))

  return (
    <div>
      <div>
        <Button type="primary">
          <Link to="/books/new">
            <i className="fas fa-plus"></i>{''}
            New Book
        </Link>
        </Button>
      </div>
      <div>
        <h2>
          <i className="fas fa-book"></i>{''} Books
        </h2>
      </div>
      <Table
        columns={columns}
        dataSource={data}
        pagination={false}
        bordered>

      </Table>
    </div>
  );
}

export default compose(
  firestoreConnect([{ collection: 'books' }]),
  connect((state, props) => ({
    books: state.firestore.ordered.books
  })
  )
)(Books);