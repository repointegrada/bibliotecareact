import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { Link } from 'react-router-dom'
import { Form, Input, Button } from 'antd';
import Spinner from '../layout/Spinner'

class EditBook extends Component {
  titleInput = React.createRef()
  editorialInput = React.createRef()
  ISBNInput = React.createRef()
  existenceInput = React.createRef()

  editSub = e => {
    e.preventDefault()
    const updatedBook = {
      title: this.titleInput.current.state.value,
      editorial: this.editorialInput.current.state.value,
      ISBN: this.ISBNInput.current.state.value,
      existence: this.existenceInput.current.state.value
    }

    const { book, firestore, history } = this.props

    firestore.update({
      collection: 'books',
      doc: book.id
    }, updatedBook)
      .then(() => history.push('/'))
  }

  render() {
    const { book } = this.props

    if (!book) return <Spinner />

    return (
      <div>
        <Button type="primary">
          <Link to="/">
            <i className="fas fa-arrow-circle-left"></i>{''}
            Go back to the list
          </Link>
        </Button>

        <h2>
          <i className="fas fa-book"></i>
          Edit book
        </h2>

        <Form onSubmit={this.editSub}>
          <Form.Item label="Title">
            <Input
              placeholder="Title or name of the book"
              type="text"
              name="title"
              ref={this.titleInput}
              defaultValue={book.title}
            />
          </Form.Item>
          <Form.Item label="ISBN">
            <Input
              placeholder="ISBN"
              type="text"
              name="ISBN"
              ref={this.ISBNInput}
              defaultValue={book.ISBN}
            />
          </Form.Item>
          <Form.Item label="Editorial">
            <Input
              placeholder="Editorial of the book"
              type="text"
              name="editorial"
              ref={this.editorialInput}
              defaultValue={book.editorial}
            />
          </Form.Item>
          <Form.Item label="Existence">
            <Input
              placeholder="Existence of the book"
              type="number"
              min="0"
              name="existence"
              ref={this.existenceInput}
              defaultValue={book.existence}
            />
          </Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button">
            Accept
        </Button>
        </Form>
      </div>
    );
  }
}

export default compose(
  firestoreConnect(props => [
    {
      collection: 'books',
      storeAs: 'book',
      doc: props.match.params.id
    }
  ]),
  connect(({
    firestore: { ordered }
  }, props) => ({
    book: ordered.book && ordered.book[0]
  }))
)(EditBook);