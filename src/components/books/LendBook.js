import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { Link } from 'react-router-dom'
import { Form, Input, Button } from 'antd';
import Spinner from '../layout/Spinner'
import CardSubscriber from '../../subscribers/CardSubscriber'
import { searchUser } from '../../redux/actions/searchUserActions'

class LendBook extends Component {
  state = {
    search: '',
    noResultado: false
  }

  searchStudent = e => {
    e.preventDefault()
    const { search } = this.state
    const { firestore, searchUser } = this.props

    const collection = firestore.collection('subscribers')
    const query = collection.where("code", "==", search).get() // Select

    query.then(resultado => {
      if (resultado.empty) {
        searchUser({})
        this.setState({
          noResultado: true
        })
      } else {
        const datos = resultado.docs[0]
        searchUser(datos.data())
        this.setState({
          noResultado: false,
        })
      }
    })

  }

  readData = e => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  // Revisar este método
  soliciteLoan = () => {
    const { user } = this.props

    user.requestDate = new Date().toLocaleDateString()
    let prestados = []

    prestados = [...this.props.book.borrowed, user]


    const libro = { ...this.props.book }

    delete libro.borrowed

    libro.borrowed = prestados

    const { firestore, history } = this.props

    firestore.update({
      collection: 'books',
      doc: libro.id
    }, libro)
      .then(() => history.push('/'))

  }
  render() {
    const { book } = this.props
    if (!book) return <Spinner />

    const { user } = this.props

    let cardStudent, btnSolicite

    if (user.name) {
      cardStudent = <CardSubscriber
        student={user} />

      btnSolicite = <Button type="primary" onClick={this.soliciteLoan}> Solicite loan
      </Button>
    } else {
      cardStudent = null
      btnSolicite = null
    }

    const { noResultado } = this.state
    let mensajeResultado = ''
    if (noResultado) {
      mensajeResultado = <div>No hay resultados para ese código</div>
    } else {
      mensajeResultado = null
    }

    return (
      <div>
        <div>
          <Button type="primary">
            <Link to="/">
              <i className="fas fa-arrow-circle-left"></i>{''}
              Go back to the list
          </Link>
          </Button>

          <h2>
            <i className="fas fa-book"></i>
            Solicit book: {book.title}
          </h2>

          <Form onSubmit={this.searchStudent}>
            <Form.Item label="Search the subscriptor by code">
              <Input
                type="text"
                name="search"
                onChange={this.readData}
              />
            </Form.Item>
            <Button type="primary" htmlType="submit" value="Search subscriptor">
              Accept
        </Button>
          </Form>

          {cardStudent}
          {btnSolicite}

          {mensajeResultado}
        </div>
      </div>
    );
  }
}

export default compose(
  firestoreConnect(props => [
    {
      collection: 'books',
      storeAs: 'book',
      doc: props.match.params.id
    }
  ]),
  connect(({
    firestore: { ordered }, user }, props) => ({
      book: ordered.book && ordered.book[0],
      user: user
    }), { searchUser })
)(LendBook);