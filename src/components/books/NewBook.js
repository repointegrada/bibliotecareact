import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Form, Input, Button } from 'antd';
import { firestoreConnect } from 'react-redux-firebase'

class NewBook extends Component {
  state = {
    title: '',
    ISBN: '',
    editorial: '',
    existence: ''
  }

  addBook = e => {
    e.preventDefault()
    const newBook = this.state

    newBook.borrowed = []
    const { firestore, history } = this.props

    firestore.add({
      collection: 'books',
    }, newBook)
      .then(() => history.push('/'))
  }

  readData = e => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }


  render() {
    return (
      <div>
        <div>
          <Button type="primary">
            <Link to={"/"}>
              <i className="fas fa-arrow-circle-left"></i> {''} Go back to the books
        </Link>
          </Button>
        </div>

        <div>
          <h2>
            <i className="fas fa-book"> </i> {''} New book
          </h2>

          <Form onSubmit={this.addBook}>
            <Form.Item label="Title">
              <Input
                placeholder="Title or name of the book"
                type="text"
                name="title"
                onChange={this.readData}
                value={this.state.title}
              />
            </Form.Item>
            <Form.Item label="ISBN">
              <Input
                placeholder="ISBN"
                type="text"
                name="ISBN"
                onChange={this.readData}
                value={this.state.ISBN}
              />
            </Form.Item>
            <Form.Item label="Editorial">
              <Input
                placeholder="Editorial of the book"
                type="text"
                name="editorial"
                onChange={this.readData}
                value={this.state.editorial}
              />
            </Form.Item>
            <Form.Item label="Existence">
              <Input
                placeholder="Existence of the book"
                type="number"
                min="0"
                name="existence"
                onChange={this.readData}
                value={this.state.existence}
              />
            </Form.Item>
            <Button type="primary" htmlType="submit" className="login-form-button">
              Accept
        </Button>
          </Form>

        </div>
      </div>
    );
  }
}

export default firestoreConnect()(NewBook);