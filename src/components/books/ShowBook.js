import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { Button } from 'antd';
import { Link } from 'react-router-dom'
import Spinner from '../layout/Spinner'


class ShowBook extends Component {

  returnBook = id => {
    const { firestore, history } = this.props
    const updatedBook = { ...this.props.book }

    const borrowed = updatedBook.borrowed.filter(element => element.code !== id)
    updatedBook.borrowed = borrowed

    firestore.update({
      collection: 'books',
      doc: updatedBook.id
    }, updatedBook)
      .then(() => history.push('/'))
  }
  render() {
    const { book } = this.props
    if (!book) return <Spinner />

    let btnLoan;

    if (book.existence - book.borrowed.length > 0) {
      btnLoan =
        <Button type="primary">
          <Link to={`/books/loan/${book.id}`}>Solicit loan</Link>
        </Button>
    } else {
      btnLoan = null
    }

    return (
      <div>
        <Button type="primary">
          <Link to="/">
            <i className="fas fa-arrow-circle-left"></i>{''}
            Go back to the list
          </Link>
        </Button>

        <Button type="primary">
          <Link to={`/books/edit/${book.id}`} >
            <i className="fas fa-pencil-alt"></i> {''}
            Edit book
          </Link>
        </Button>

        <hr />

        <div>
          <h2>{book.title}</h2>
          <p>
            <span>ISBN:</span>{''}
            {book.ISBN}
          </p>

          <p>
            <span>Editorial:</span>{''}
            {book.editorial}
          </p>

          <p>
            <span>Availables:</span>{''}
            {book.existence - book.borrowed.length}
          </p>

          {/** btn for solicite a loan */}
          {btnLoan}
          <h3>People who have loaned books</h3>
          {book.borrowed.length > 1 ? book.borrowed.map(prestado => (
            <div key={prestado.code}>
              <h4>
                {prestado.name} {prestado.lastname}
              </h4>

              <p>
                <span>Code: </span>{''}
                {prestado.code}
              </p>

              <p>
                <span>Carrer: </span>{''}
                {prestado.career}
              </p>

              <p>
                <span>Request Date: </span>{''}
                {prestado.requestDate}
              </p>

              <Button type="primary" onClick={() => this.returnBook(prestado.code)}>
                Book return
              </Button>
            </div>
          )) : null}
        </div>
      </div>
    );
  }
}

export default compose(
  firestoreConnect(props => [
    {
      collection: 'books',
      storeAs: 'book',
      doc: props.match.params.id
    }
  ]),
  connect(({
    firestore: { ordered }
  }, props) => ({
    book: ordered.book && ordered.book[0]
  }))
)(ShowBook);