import React, { Component } from 'react'
import { Button, Layout, Menu } from 'antd';
import './NavBar.scss'
import 'antd/dist/antd.css';
import { Route, Link } from "react-router-dom";
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'

const { Header } = Layout;

class Navbar extends Component {
  state = {
    isAuthenticated: false
  }

  static getDerivedStateFromProps(nextProps) {

    const { auth } = nextProps
    if (auth) {
      return { isAuthenticated: true }
    } else {
      return { isAuthenticated: false }
    }
  }

  signOut = e => {
    const { firebase } = this.props
    firebase.logout()
  }

  render() {
    const { isAuthenticated } = this.state
    const { auth } = this.props

    return (
      <Layout>
        {isAuthenticated ?
          (<Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
            <Menu
              theme="dark"
              mode="horizontal"
              defaultSelectedKeys={['2']}
              style={{ lineHeight: '64px' }}
            >
              <Route>
                <Link to={'/subscribers'}>Subscribers </Link>
                <Link to={'/'}>Books</Link>
              </Route>
            </Menu>
          </Header>
          )
          : null}

        {isAuthenticated ? (
          <div>
            <Route>
              <Link to={'#!'}>{auth.email} </Link>
            </Route>
            <Button type="primary" htmlType="submit" value="Sign out" onClick={this.signOut}>
              Sign out
    </Button>
          </div>
        ) : null}
      </Layout>
    );
  }
}

export default compose(
  firebaseConnect(),
  connect((state, props) => ({
    auth: state.firebase.auth

  }))
)(Navbar);
