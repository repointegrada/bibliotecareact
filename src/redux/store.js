import { createStore, combineReducers, compose } from 'redux'
import { reactReduxFirebase, firebaseReducer } from 'react-redux-firebase'
import { reduxFirestore, firestoreReducer } from 'redux-firestore'
import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

import searchUserReducer from './reducers/searchUserReducer'

const firebaseConfig = {
  apiKey: "AIzaSyCuvt_5CMXGJR8zo7qeqQnQTjGze4SeVW8",
  authDomain: "bibliostore-c6a80.firebaseapp.com",
  databaseURL: "https://bibliostore-c6a80.firebaseio.com",
  projectId: "bibliostore-c6a80",
  storageBucket: "",
  messagingSenderId: "392581468418",
  appId: "1:392581468418:web:c7c626946dc68b6288b21b"
};

// Iniciar firebase
firebase.initializeApp(firebaseConfig);

const rrfConfig = {
  userProfile: 'users',
  useFirestoreForProfile: true
}

// enhancer con compose con redux y firestore
const createStoreWithFirebase = compose(
  reactReduxFirebase(firebase, rrfConfig),
  reduxFirestore(firebase)
)(createStore)

// Reducers
const rootReducer = combineReducers({
  firebase: firebaseReducer,
  firestore: firestoreReducer,
  user: searchUserReducer
})

const initialState = {}

const store = createStoreWithFirebase(rootReducer, initialState, compose(
  reactReduxFirebase(firebase),
  // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
))

export default store