import { createStore, combineReducers, compose } from 'redux'
import { reactReduxFirebase, firebaseReducer } from 'react-redux-firebase'
import { reduxFirestore, firestoreReducer } from 'redux-firestore'
import firebase from 'firebase/app'
import 'firebase/firestore'

const firebaseConfig = {
  apiKey: "AIzaSyBA0WMbtqggdv2I6a58XyjVCbRlxzScV_8",
  authDomain: "bibliostore-e3ce5.firebaseapp.com",
  databaseURL: "https://bibliostore-e3ce5.firebaseio.com",
  projectId: "bibliostore-e3ce5",
  storageBucket: "bibliostore-e3ce5.appspot.com",
  messagingSenderId: "99593231338",
  appId: "1:99593231338:web:f851e669709e0fcfdbf093"
};

// Iniciar firebase
firebase.initializeApp(firebaseConfig);

const rrfConfig = {
  userProfile: 'users',
  useFirestoreForProfile: true
}

// enhancer con compose con redux y firestore
const createStoreWithFirebase = compose(
  reactReduxFirebase(firebase, rrfConfig),
  reduxFirestore(firebase)
)(createStore)

// Reducers
const rootReducer = combineReducers({
  firebase: firebaseReducer,
  firestore: firestoreReducer
})

const initialState = {}

const store = createStoreWithFirebase(rootReducer, initialState, compose(
  reactReduxFirebase(firebase),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
))

export default store