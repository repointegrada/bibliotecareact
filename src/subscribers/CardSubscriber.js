import React from 'react'


const CardSubscriber = ({ student }) => {
  return (
    <div>
      <h3>Datos del solicitante</h3>
      <div>
        <p>Name: {''}
          <span>{student.name}</span>
        </p>

        <p>Code: {''}
          <span>{student.code}</span>
        </p>

        <p>Career: {''}
          <span>{student.career}</span>
        </p>
      </div>
    </div>
  );
}

export default CardSubscriber;