import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { Form, Input, Button } from 'antd';
import { Link } from 'react-router-dom'
import Spinner from '../components/layout/Spinner'

class EditSubscriber extends Component {

  nameInput = React.createRef()
  lastnameInput = React.createRef()
  careerInput = React.createRef()
  codeInput = React.createRef()

  editSub = e => {
    e.preventDefault()
    const updatedSub = {
      name: this.nameInput.current.state.value,
      lastname: this.lastnameInput.current.state.value,
      career: this.careerInput.current.state.value,
      code: this.codeInput.current.state.value
    }


    const { suscriptor, firestore, history } = this.props

    firestore.update({
      collection: 'subscribers',
      doc: suscriptor.id
    }, updatedSub)
      .then(() => history.push('/subscribers'))
  }

  render() {
    const { suscriptor } = this.props
    if (!suscriptor) return <Spinner />

    return (
      <div>
        <div>
          <Link to={'/subscribers'} >
            <i className="fas fa-arrow-circle-left"></i> {''} Go back to list
          </Link>
        </div>

        <div>

          <h2>
            <i className="fas fa-user-plus"></i>
            Edit Sub
          </h2>

          <Form onSubmit={this.editSub}>
            <Form.Item label="Name">
              <Input
                placeholder="Name of subscriber"
                type="text"
                name="name"
                ref={this.nameInput}
                defaultValue={suscriptor.name}
              />
            </Form.Item>
            <Form.Item>
              <Input
                type="text"
                name="lastname"
                placeholder="Last Name"
                ref={this.lastnameInput}
                defaultValue={suscriptor.lastname}
              />
            </Form.Item>
            <Form.Item>
              <Input
                type="text"
                name="career"
                placeholder="Career"
                ref={this.careerInput}
                defaultValue={suscriptor.career}
              />
            </Form.Item>
            <Form.Item>
              <Input
                type="text"
                name="code"
                placeholder="Code"
                ref={this.codeInput}
                defaultValue={suscriptor.code}
              />
            </Form.Item>
            <Button type="primary" htmlType="submit" className="login-form-button">
              Accept
        </Button>
          </Form>
        </div>
      </div>

    );
  }
}

export default compose(
  firestoreConnect(props => [
    {
      collection: 'subscribers',
      storeAs: 'suscriptor',
      doc: props.match.params.id
    }
  ]),
  connect(({ firestore: { ordered } }, props) => ({
    suscriptor: ordered.suscriptor && ordered.suscriptor[0]
  }))
)(EditSubscriber);
