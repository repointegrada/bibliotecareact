import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Form, Input, Button } from 'antd';
import { firestoreConnect } from 'react-redux-firebase'

class NewSubscriber extends Component {
  state = {
    name: '',
    lastname: '',
    career: '',
    code: ''
  }

  addSub = e => {
    e.preventDefault()
    const newSub = this.state
    const { firestore, history } = this.props

    firestore.add({
      collection: 'subscribers',
    }, newSub)
      .then(() => history.push('/subscribers'))
  }

  readData = e => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }


  render() {
    return (
      <div>
        <Link to={'/subscribers'} >
          <i className="fas fa-arrow-circle-left"></i> {''} Go back to list
      </Link>

        <Form onSubmit={this.addSub}>
          <Form.Item label="Name">
            <Input
              placeholder="Name of subscriber"
              type="text"
              name="name"
              onChange={this.readData}
              value={this.state.name}
            />
          </Form.Item>
          <Form.Item label="Last name">
            <Input
              type="text"
              name="lastname"
              placeholder="Last Name"
              onChange={this.readData}
              value={this.state.lastname}
            />
          </Form.Item>
          <Form.Item label="Career">
            <Input
              type="text"
              name="career"
              placeholder="Career"
              onChange={this.readData}
              value={this.state.career}
            />
          </Form.Item>
          <Form.Item label="Code">
            <Input
              type="text"
              name="code"
              placeholder="Code"
              onChange={this.readData}
              value={this.state.code}
            />
          </Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button">
            Add sub
          </Button>
        </Form>

      </div>
    );
  }
}

export default firestoreConnect()(NewSubscriber);
