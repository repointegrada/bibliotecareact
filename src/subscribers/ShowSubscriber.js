import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { Button } from 'antd';
import { Link } from 'react-router-dom'
import Spinner from '../components/layout/Spinner'

const ShowSubscriber = ({ suscriptor }) => {

  if (!suscriptor) return <Spinner />
  return (
    <div>
      <div>
        <Button type="primary">
          <Link to="/subscribers">
            <i className="fas fa-arrow-circle-left"></i>
          </Link>
        </Button>
      </div>

      <div>
        <Link to={`/subscribers/edit/${suscriptor.id}`} >
          <i className="fas fa-pencil-alt"></i> {''}
        </Link>
      </div>

      <hr className="mx-5 w-100" />

      <div className="col-12">
        <h2 className="mb-4">
          {suscriptor.name} {suscriptor.lastname}
        </h2>

        <p>
          <span>
            Carrera: {suscriptor.career}
          </span>

          <span>
            Code: {suscriptor.code}
          </span>
        </p>
      </div>
    </div>
  )
}

export default compose(
  firestoreConnect(props => [
    {
      collection: 'subscribers',
      storeAs: 'suscriptor',
      doc: props.match.params.id
    }
  ]),
  connect(({
    firestore: { ordered }
  }, props) => ({
    suscriptor: ordered.suscriptor && ordered.suscriptor[0]
  }))
)(ShowSubscriber);

