import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { Button, Table } from 'antd';
import { Link } from 'react-router-dom'
import Spinner from '../components/layout/Spinner'


const Subscriptor = ({ subscriptors, firestore, history }) => {

  if (!subscriptors) return <Spinner />

  /**
   * Se especifíca a que tabla y con que id se va a eliminar el registro
   * @param {*} id 
   */
  const deleteSub = id => {
    firestore.delete({
      collection: 'subscribers',
      doc: id
    })
      .then(history.push('/subscribers'))
  }

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Career',
      dataIndex: 'career',
      key: 'career',
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      render: (text, data) => (
        <div>
          <Button type="primary">
            <Link to={`/subscribers/show/${data.key}`}>
              <i className="fas fa-angle-double-right"></i> {''} More Info
        </Link>
          </Button>

          <Button type="danger" onClick={() => deleteSub(data.key)}>
            <i className="fas fa-trash-alt"></i> {' '} Delete
        </Button>
        </div>
      ),
    }
  ];

  const data = subscriptors.map(subs => ({
    key: subs.id,
    name: subs.name,
    career: subs.career
  }))

  return (
    <div>
      <div>
        <Button type="primary">
          <Link to='/subscribers/new'>
            <i className="fas fa-plus"></i> {''} New subscriber
        </Link>
        </Button>
      </div>
      <div>
        <h2> <i className="fas fa-users"></i> Subscribers</h2>
      </div>

      <Table
        columns={columns}
        dataSource={data}
        pagination={false}
        bordered
      // footer={() => 'Footer'}
      // loading={this.state.loading}
      // size="middle"
      // title={() => 'Header'}
      />
    </div>
  );
}

export default compose(
  firestoreConnect([{ collection: 'subscribers' }]),
  connect((state, props) => ({
    subscriptors: state.firestore.ordered.subscribers
  })
  )
)(Subscriptor);